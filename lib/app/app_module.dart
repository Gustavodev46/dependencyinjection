import 'package:flutter_modular/flutter_modular.dart';
import 'package:slidy_aula_1/app/modules/bas/bas_module.dart';

import 'modules/others/others_module.dart';

class AppModule extends Module {
  @override
  final List<Module> imports = [
    BasModule(),
  ];
  @override
  final List<Bind> binds = [
    Bind.instance<String>('Gustavo'),
    Bind.lazySingleton((i) => Controller(i<String>(), i<bool>())),
  ];
}
